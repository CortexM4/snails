﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {

	public Image imageWind;
	public Text forceWindText;
	public Text countSnails;
	public Text enemySnails;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		forceWindText.text = Wind.Instance.getForceWindText() + "m/s";
		imageWind.rectTransform.rotation = Wind.Instance.getRotationWind();

		countSnails.text = ScoreManager.Instance.getCountSnails().ToString();
		enemySnails.text = ScoreManager.Instance.getEnemySnails().ToString();
	}
}

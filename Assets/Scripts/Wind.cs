﻿using UnityEngine;
using System.Collections;

public class Wind : MonoBehaviour {

	public static Wind Instance { get; private set;}
	public int sec_min = 10;
	public int sec_max = 30;
	public float forceWind = 1f;

	int force_min = 1;
	int force_max = 7;
	int force;
	Vector2 forceWindVector;
	int angularWind;

	void Awake() {
		Instance = this;
	}
	// Use this for initialization
	void Start () {
		forceWindVector = Vector2.zero;
		angularWind = 0;
		force = 0;
		StartCoroutine(RandomWindTime());
	}

	/*
	 * 	Тут все в обратную сторону: Вектор - угол поворота
	 *  (-1, 0) = 0
	 * 	(0, -1) = 90
	 * 	(1, 0) = 180
	 * 	(0, 1) = 270
	 */
	public Vector2 getForceWind() {
		return forceWindVector;
	}

	public float getForceWindText() {
		return force*forceWind;
	}

	public Quaternion getRotationWind() {
		return Quaternion.Inverse(Quaternion.Euler(0,0,angularWind));
	}

	void RandomWindVector() {
		angularWind = Random.Range(1, 360);
		force = Random.Range(force_min, force_max);
		Vector2 wind = Quaternion.Euler(new Vector3(0,0,angularWind)) * -Vector2.right;
		forceWindVector = new Vector2(wind.x, wind.y*(-1)) * (force*forceWind);
	}

	IEnumerator RandomWindTime() {
		while(true) {
			RandomWindVector();
			yield return new WaitForSeconds(Random.Range(sec_min, sec_max));
		}
	}
}

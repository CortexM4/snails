﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SnailManager : MonoBehaviour {

	public float speed = 0.05f;
	public GameObject abilityPanel;
	public GameObject miniMapUsed;

	bool isMoved;
	// Use this for initialization
	void Start () {
		isMoved = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

	void FixedUpdate () {
		if(isMoved) {
			transform.position = new Vector2(transform.position.x + speed, transform.position.y);
			if(!miniMapUsed.GetComponent<MiniMapControl>().isPlayerMouseDown) {
				if (transform.position.x > Camera.main.transform.position.x) {
					Camera.main.GetComponent<FireCamManagment> ().isUsed = true;
					Camera.main.transform.position = new Vector3 (transform.position.x, Camera.main.transform.position.y,
					                                              Camera.main.transform.position.z);
				}
			}
		}
	}

	void OnMouseDown() {
		//isClicked = true;
		abilityPanel.SetActive(true);
		miniMapUsed.GetComponent<MiniMapControl> ().isPlayerMouseDown = false;

	}

	public void LetsMove() {
		isMoved = true;
		abilityPanel.SetActive(false);
	}

	public void LetStop() {
		isMoved = false;
		abilityPanel.SetActive(false);
	}

}

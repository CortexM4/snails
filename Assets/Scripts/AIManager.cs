﻿using UnityEngine;
using System.Collections;

public class AIManager : MonoBehaviour {

	public GunManager gun;

	Vector2[] matrix;
	// Use this for initialization
	void Start () {
		matrix = new Vector2[]
		{
			//new Vector2(-1.0f, 0.3f),
			//new Vector2(-1.0f, 0.2f),
			//new Vector2(-1.0f, 0.1f),
			new Vector2(-1.0f, 0.075f),
			new Vector2(-1.0f, 0.065f),
			new Vector2(-1.0f, 0.08f),
			new Vector2(-1.0f, 0.06f),
			new Vector2(-1.0f, 0.05f),
			new Vector2(-1.0f, 0.09f)
		};

	}
	
	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate() {
		if(!gun.isReload()) {
			Vector2 aiVector = matrix[Random.Range(0, matrix.Length-1)];
			gun.setDirectionFire(aiVector);
			gun.Fire();
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public static ScoreManager Instance {get; private set;}

	public int countSnails = 100;
	public int enemySnails = 100;

	void Awake () {
		Instance = this;
	}

	public int getCountSnails() {
		return countSnails;
	}

	public void removeCountSnails(int count) {
		countSnails -= count;
	}

	public int getEnemySnails() {
		return enemySnails;
	}

	public void removeEnemySnails(int count) {
		enemySnails -= count;
	}
}

﻿using UnityEngine;
using System.Collections;

public class FireCamManagment : MonoBehaviour {

	public GameObject ball;
	public bool isUsed;		// Флаг используется для возвращения камеры после выстрела если true
							// если false при изменении координат камеры, она не возвращается
	float x, y, z;
	int speedReturn = 5;
	void Start () {
		x = transform.position.x;
		y = transform.position.y;
		z = transform.position.z;
		isUsed = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		/*if(Input.GetMouseButtonDown(0))
		{
			RaycastHit2D hit;
			hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -17.7f)), 
			                        Vector2.zero);
			if(hit.collider != null)
			{
				Debug.Log ("Target Position: " + hit.collider.gameObject.transform.position);
			}
		}*/
		if (!isUsed) {
			if (transform.position.x > x) {
				float currentX = ((transform.position.x - speedReturn) > x) ? transform.position.x - speedReturn :
					transform.position.x - 1;
				transform.position = new Vector3 (currentX, y, z);
			}
		}
	}

}

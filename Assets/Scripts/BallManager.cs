﻿using UnityEngine;
using System.Collections;

public class BallManager : MonoBehaviour {


	public bool isPlayerBall;
	Rigidbody2D rb2;
	Wind forceWind;
	// Use this for initialization
	void Awake () {
		//fireCam = Camera.main;
	}
	void Start () {
		rb2 = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isPlayerBall) {
			if (transform.position.x > Camera.main.transform.position.x) {
				Camera.main.GetComponent<FireCamManagment> ().isUsed = true;
				Camera.main.transform.position = new Vector3 (transform.position.x, Camera.main.transform.position.y,
				                                              Camera.main.transform.position.z);
			}
		}
	}

	void FixedUpdate() {
		// Добавление силы ветра. Гребаное надувательство. Сила ветра влияет только на игрока
		// могут заметить и яйца оторвать!!!!!!
		if(isPlayerBall)
			rb2.AddForce(Wind.Instance.getForceWind());
		// Это полная херня (Удаление должно быть по триггеру)
		if (transform.position.x > 105 || transform.position.x < -130) {
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		Destroy(gameObject);
	}

	void OnDestroy() {
		if (isPlayerBall) {
			Camera.main.GetComponent<FireCamManagment> ().isUsed = false;
		}
	}
}

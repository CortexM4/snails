﻿using UnityEngine;
using System.Collections;

public class MiniMapControl : MonoBehaviour {

	public bool isPlayerMouseDown;
	Camera MiniMapCamera;
	bool mouseDown;

	void Start() {
		MiniMapCamera = GetComponent<Camera> ();
		mouseDown = false;
		StartCoroutine(TranslateMap());
	}

	void FixedUpdate() {
		if (Input.GetMouseButtonDown (0)) {
			mouseDown = true;
			Camera.main.GetComponent<FireCamManagment> ().isUsed = true;
		}
		if (Input.GetMouseButtonUp (0)) {
			mouseDown = false;
		}
	}

	IEnumerator TranslateMap() {
		while (true) {
			if (mouseDown) {
				if (MiniMapCamera.pixelRect.Contains (Input.mousePosition)) {
					isPlayerMouseDown = true;
					Ray MouseRay = MiniMapCamera.ScreenPointToRay (Input.mousePosition);
					Camera.main.transform.position = new Vector3 (MouseRay.origin.x, Camera.main.transform.position.y, 
				                                              Camera.main.transform.position.z);
				}
				else
					mouseDown = false;
			}
			yield return null;
		}
	}
}

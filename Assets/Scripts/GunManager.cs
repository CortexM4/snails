﻿using UnityEngine;
using System.Collections;

public class GunManager : MonoBehaviour {
	
	public BallManager instanceBall;
	public float force = 6000f;
	public Rigidbody2D ball;
	public bool player;
	public int timeReload = 3;
	public int removeSnailsCount = 3;

	int direction; 				// Направление оружия применимо к scale (-1 - смотрит влево, 1 - на право)
	bool isReloaded;
	float magnitude = 5f;		// Длина стрелки наведения
	float angle = 30f;			// Угол на который пушка может стрелять
	Vector2 vTrack;
	bool isMousePressed;
	LineRenderer line;
	Vector2 position;
	float delta = 1f;			// отступ срелки наведения от спрайта пушки

	// Use this for initialization
	void Start () {
		if (player)
			direction = 1;
		else
			direction = -1;
		Vector3 theScale = transform.localScale;
		theScale.x *= direction;
		transform.localScale = theScale;
		isMousePressed = false;
		isReloaded = false;
		line = GetComponent<LineRenderer>();
		line.SetColors(Color.green, Color.red);
		line.SetWidth(0.2f, 00f);
		position = transform.position;
	}

	/*
	 * Выстрел и прицеливание ведется мышью
	 */

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp (0)) {
			if (!isMousePressed)
				return;
			isMousePressed = false;
			if(!isReloaded) {
				Fire();
			}
			line.SetPosition(1, new Vector2(position.x + delta*direction, position.y));
		}

		if(isMousePressed)
		{
			Vector3 mouse = Input.mousePosition;
			mouse.z = Camera.main.transform.position.z * (-1);
			Vector2 mousePosition = Camera.main.ScreenToWorldPoint(mouse);
			if(Vector2.Angle(Vector2.right*direction, (mousePosition - position)) > angle) {
				line.SetPosition(0, new Vector2(position.x+delta*direction, position.y));
				return;
			}
			vTrack = mousePosition - position;
			Vector2 draw = Vector2.ClampMagnitude(vTrack, magnitude) + position;
			line.SetPosition(1, draw);
		}
		
	}

	public bool isReload() {
		return isReloaded;
	}

	// Эта функция относится только к AI (вызов из AIManager)
	public void setDirectionFire(Vector2 dir) {
		vTrack = dir;
	}

	public void Fire() {
		Rigidbody2D clone = Instantiate(ball, new Vector2(position.x+delta*direction, position.y), 
		                                Quaternion.identity) as Rigidbody2D;
		//if (player) {
		//	clone.GetComponent<BallManager> ().fireCam = Camera.main;
		//}
		clone.GetComponent<BallManager>().isPlayerBall = player;
		vTrack.Normalize();
		
		clone.AddForce( vTrack * force );
		//clone.AddRelativeForce( vTrack * force );
		if(player)
			ScoreManager.Instance.removeCountSnails(1);
		else
			ScoreManager.Instance.removeEnemySnails(1);

		isReloaded = true;
/*
* Тут узкое место, надо протестить, может вместо корутины deltaTime использовать!!
*/
		StartCoroutine(Reload());
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(player)
			ScoreManager.Instance.removeCountSnails(removeSnailsCount);
		else
			ScoreManager.Instance.removeEnemySnails(removeSnailsCount);

	}

	void OnMouseDown()
	{
		line.SetPosition(0, new Vector2(position.x+delta*direction, position.y));
		line.SetPosition(1, new Vector2(position.x+delta*direction, position.y));
		isMousePressed = true;
	}

	IEnumerator Reload() {
		//print("Reloaded");
		yield return new WaitForSeconds(timeReload);
		//print("Done");
		isReloaded = false;

	}
}